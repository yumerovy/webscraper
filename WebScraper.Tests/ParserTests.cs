using Microsoft.VisualStudio.TestTools.UnitTesting;
using WebScraper;
using HtmlAgilityPack;
using System;
using System.Linq;

namespace WebScraper.Tests
{
    [TestClass]
    public class ParserTests
    {
        [TestMethod]
        public void ParseProducts_ShouldParseProductsCorrectly()
        {
            // Arrange
            var htmlDoc = new HtmlDocument();
            htmlDoc.Load("C:\\Training\\FlatRock task\\WebScraper\\WebScrapper\\task html.html");

            // Act
            var products = HtmlParser.ParseProducts(htmlDoc);

            // Assert
            Assert.IsNotNull(products);
            Assert.IsTrue(products.Count > 0);
        }

        [TestMethod]
        public void ParseProducts_ShouldParseProductNameCorrectly()
        {
            var htmlDoc = new HtmlDocument();
            htmlDoc.Load("C:\\Training\\FlatRock task\\WebScraper\\WebScrapper\\task html.html");

            var products = HtmlParser.ParseProducts(htmlDoc);

            Assert.AreEqual("Electrolux 700L Chest Freezer & Filter", products[0].Name);
        }

        [TestMethod]
        public void ParseProducts_ShouldParseProductPriceCorrectly()
        {
            var htmlDoc = new HtmlDocument();
            htmlDoc.Load("C:\\Training\\FlatRock task\\WebScraper\\WebScrapper\\task html.html");

            // Act:
            var products = HtmlParser.ParseProducts(htmlDoc);

            // Assert:
            Assert.AreEqual(22.99, products[1].Price, 0.01); // Allow for a small difference due to rounding
        }

        [TestMethod]
        public void ParseProducts_ShouldParseProductRatingCorrectly()
        {
            // Arrange:
            var htmlDoc = new HtmlDocument();
            htmlDoc.Load("C:\\Training\\FlatRock task\\WebScraper\\WebScrapper\\task html.html");

            // Act:
            var products = HtmlParser.ParseProducts(htmlDoc);

            // Assert: 
            Assert.AreEqual(4.2, products[2].Rating, 0.01); 
        }

    }
}
