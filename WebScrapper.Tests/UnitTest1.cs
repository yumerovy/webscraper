﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using WebScraper;
using HtmlAgilityPack;
using System;

namespace WebScraper.Tests
{
    [TestClass]
    public class UnitTest1
    {
            [TestMethod]
            public void ParseProducts_ShouldParseProductsCorrectly()
            {
                // Arrange
                var htmlDoc = new HtmlDocument();
                htmlDoc.LoadHtml(@"<!-- Your HTML content here -->");

                // Act
                var products = HtmlParser.ParseProducts(htmlDoc);

                // Assert
                Assert.IsNotNull(products);
                Assert.IsTrue(products.Count > 0);

                // Add more assertions based on your expectations
                // For example, check if product names, prices, and ratings are correctly parsed.
            }

        [TestMethod]
        public void ParseProducts_ShouldParseProductsCorrectly()
        {
            // Arrange: Create an HTML document for testing
            var htmlContent = "<!-- Your HTML content here -->";
            var htmlDoc = new HtmlDocument();
            htmlDoc.LoadHtml(htmlContent);

            // Act: Parse products
            var products = HtmlParser.ParseProducts(htmlDoc);

            // Assert: Check if products are correctly parsed
            Assert.IsNotNull(products);
            Assert.IsTrue(products.Count > 0);

            // Add more assertions based on your expectations
            // For example, check if product names, prices, and ratings are correctly parsed.
        }

        [TestMethod]
        public void ParseProducts_ShouldParseProductNameCorrectly()
        {
            // Arrange: Create an HTML document with a known product name
            var htmlContent = "<!-- HTML content with a known product name -->";
            var htmlDoc = new HtmlDocument();
            htmlDoc.LoadHtml(htmlContent);

            // Act: Parse products
            var products = HtmlParser.ParseProducts(htmlDoc);

            // Assert: Check if the product name is correctly parsed
            Assert.AreEqual("Expected Product Name", products[0].Name);
        }

        [TestMethod]
        public void ParseProducts_ShouldParseProductPriceCorrectly()
        {
            // Arrange: Create an HTML document with a known product price
            var htmlContent = "<!-- HTML content with a known product price -->";
            var htmlDoc = new HtmlDocument();
            htmlDoc.LoadHtml(htmlContent);

            // Act: Parse products
            var products = HtmlParser.ParseProducts(htmlDoc);

            // Assert: Check if the product price is correctly parsed
            Assert.AreEqual(19.99, products[0].Price, 0.01); // Allow for a small difference due to rounding
        }

        [TestMethod]
        public void ParseProducts_ShouldParseProductRatingCorrectly()
        {
            // Arrange: Create an HTML document with a known product rating
            var htmlContent = "<!-- HTML content with a known product rating -->";
            var htmlDoc = new HtmlDocument();
            htmlDoc.LoadHtml(htmlContent);

            // Act: Parse products
            var products = HtmlParser.ParseProducts(htmlDoc);

            // Assert: Check if the product rating is correctly parsed
            Assert.AreEqual(4.5, products[0].Rating, 0.01); // Allow for a small difference due to rounding
        }

    }
    }
}
