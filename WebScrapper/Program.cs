﻿using System;
using System.Net;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using HtmlAgilityPack;
using System.Web;
using System.Xml.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace WebScraper
{
    internal class Program
    {
        public static void Main()
        {
            HtmlDocument htmlDoc = new HtmlDocument();
            htmlDoc.Load("C:\\Training\\FlatRock task\\WebScraper\\WebScrapper\\task html.html");

            ICollection<Product> products = HtmlParser.ParseProducts(htmlDoc);

            //PrintResult(products);

            //foreach (Product product in products)
            //{
            //    Console.WriteLine(product);
            //}

            //Console.WriteLine(JsonConvert.SerializeObject(products, Formatting.Indented));

            var jsonOption = new JsonSerializerOptions
            {
                WriteIndented = true,
                PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
                NumberHandling = JsonNumberHandling.WriteAsString,
                DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingNull,
            };
            Console.WriteLine(System.Text.Json.JsonSerializer.Serialize(products, jsonOption));

        }

        private static void PrintResult(ICollection<Product> products)
        {
            foreach (var item in products)
            {
                Console.WriteLine(JToken.Parse(item.View()).ToString(Formatting.Indented));
            }
        }
    }
}