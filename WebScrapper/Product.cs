﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
//using JsonIgnoreAttribute = Newtonsoft.Json.JsonIgnoreAttribute;

namespace WebScraper
{
    public class Product
    {
        public Product(string name, double price, double rating)
        {
            this.Name = name;
            this.Price = price;
            //this.Rating = rating > 5 ? rating / 2 : rating;
            this.Rating = AdjustRating(rating);
        }

        [JsonProperty("productName")]
        [JsonPropertyName("productName")]
        public string Name { get; set; }


        [Newtonsoft.Json.JsonIgnoreAttribute]
        public double Price { get; set; }


        [System.Text.Json.Serialization.JsonIgnore]
        [JsonProperty("price")]
        public string FormattedPrice => Price.ToString("0.00");


        [Newtonsoft.Json.JsonIgnoreAttribute]
        public double Rating { get; set; }


        [System.Text.Json.Serialization.JsonIgnore]
        [JsonProperty("rating")]
        public string FormattedRating => Rating.ToString("0.0");


        public string View()
        {
            return $"{{\"productName\":\"{this.Name}\",\"price\":\"{this.Price:F2}\",\"rating\":\"{this.Rating}\"}}";
        }

        public override string ToString()
        {
            var options = new JsonSerializerOptions()
            {
                WriteIndented = true,
                PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
                NumberHandling = JsonNumberHandling.WriteAsString,
                DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingNull,
            };
            return System.Text.Json.JsonSerializer.Serialize(this, options);
        }

        private double AdjustRating(double originalRating)
        {
            double adjustedRating = originalRating;

            while (adjustedRating > 5) adjustedRating /= 2;

            return Math.Max(0, adjustedRating);
        }
    }
}
