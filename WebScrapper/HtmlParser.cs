﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace WebScraper
{
    public static class HtmlParser
    {
        public static Collection<Product> ParseProducts(HtmlDocument htmlDoc) 
        {
            var products = new Collection<Product>();

            #region test HtlmAgilityPAck

            //var htmlDivs = htmlDoc.DocumentNode.SelectNodes("//div/figure/a/img"); -first approach

            //var test = htmlDoc.DocumentNode.SelectNodes("//div[@class='item']").ElementAt(i);
            //Console.WriteLine("Items Count:" + test.Count);

            //foreach (var item in test)
            //{
            //    Console.WriteLine("Name:" + item.OriginalName + item.Ancestors().Where(n => n.Attributes["img"].Value == "amkXXXXX"/* opravi tuka !!!*/ ));

            //    foreach (var attrib in item.Attributes)
            //    {
            //        Console.WriteLine(attrib.OriginalName + " " + attrib.Value);
            //    }
            //}

            //Console.Write(name + ", ");

            //html / body / div[2] / figure / a / img - name xpath

            #endregion

            int prodCount = htmlDoc.DocumentNode.SelectNodes("//div[@class='item']").Count;
            for (int i = 1; i <= prodCount; i++)
            {
                var name = htmlDoc.DocumentNode.SelectSingleNode($"//div[{i}]/figure/a/img").Attributes["alt"].Value;
                name = HttpUtility.HtmlDecode(name);

                var priceString = htmlDoc.DocumentNode.SelectSingleNode($"//div[{i}]/div/div[1]/p/span/span[1]").InnerText.Substring(1);
                double.TryParse(priceString, out double price);

                //var ratingString = htmlDoc.DocumentNode.SelectSingleNode($"//div[{i}]").Attributes["rating"].Value;
                var ratingString = htmlDoc.DocumentNode.SelectNodes("//div[@class='item']").ElementAt(i - 1).Attributes["rating"].Value;
                double.TryParse(ratingString, out double rating);

                products.Add(new Product(name, price, rating));
            }

            return products;
        }

    }
}
